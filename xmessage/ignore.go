package xmessage

var ignoreMessageHeaders = map[string]struct{}{
	"ping": {},
}

func IsIgnoreMessageHeader(head string) bool {
	_, ok := ignoreMessageHeaders[head]
	return ok
}
