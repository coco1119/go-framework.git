package xmessage

type MsgSign struct {
	Time   int64 `json:"time"`
	Encode int   `json:"encode"`
}

type MsgHeader struct {
	Header  string  `json:"head"`
	Data    string  `json:"data"`
	ErrCode int16   `json:"errcode"`
	ErrMsg  string  `json:"errmsg"`
	Sign    MsgSign `json:"msgsign"`
}

type MsgBase struct {
	Uid     int64   `json:"uid"`
	IP      string  `json:"ip"`
	Head    string  `json:"head"`
	ErrCode int16   `json:"errcode"`
	Sign    MsgSign `json:"msgsign"`
	Data    string  `json:"data"`
}
