package xlock

import (
	"gitee.com/coco1119/go-framework.git/xlog"
	"runtime/debug"
	"sync"
	"time"
)

var DefaultTimeout = time.Second * 3

type RWMutex struct {
	sync.RWMutex
	closeChan chan struct{}
}

func (l *RWMutex) LockWithTimeOut(n time.Duration) bool {
	ticker := time.NewTicker(n)
	defer ticker.Stop()
	lockCh := make(chan struct{})
	go func() {
		l.Lock()
		lockCh <- struct{}{}
	}()
	select {
	case <-lockCh:
		return true
	case <-ticker.C:
		return false
	}

}

func (l *RWMutex) RlockWithTimeOut(n time.Duration) bool {
	ticker := time.NewTicker(n)
	defer ticker.Stop()
	lockCh := make(chan struct{})
	go func() {
		l.RLock()
		lockCh <- struct{}{}
	}()
	select {
	case <-lockCh:
		return true
	case <-ticker.C:
		return false
	}
}

func (l *RWMutex) RLockWithLog() {
	l.RLock()
	return
}

func (l *RWMutex) CustomLock() {

	l.Lock() //

	go func(stack string) {
	loop:
		for {
			select {
			case <-time.After(5 * time.Second):
				xlog.Logger().Errorf("get lock time out: %s", stack)
				break loop
			case <-l.closeChan:
				break loop
			}
		}
	}(string(debug.Stack()))
	l.closeChan = make(chan struct{})
	return
}

func (l *RWMutex) CustomUnLock() {
	close(l.closeChan)
	l.Unlock()
	return
}
