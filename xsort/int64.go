package xsort

type Int64Slice []int64

func (p Int64Slice) Len() int           { return len(p) }
func (p Int64Slice) Less(i, j int) bool { return p[i] < p[j] }
func (p Int64Slice) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }

type SortItem struct {
	key   int
	param interface{}
}
type SortItemSlice []SortItem

func (self SortItemSlice) Len() int {
	return len(self)
}
func (self SortItemSlice) Swap(i, j int) {
	self[i], self[j] = self[j], self[i]
}
func (self SortItemSlice) Less(i, j int) bool {
	return self[i].key < self[j].key
}
