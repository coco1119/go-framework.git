module gitee.com/coco1119/go-framework.git

go 1.16

require (
	github.com/garyburd/redigo v1.6.3
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/jinzhu/gorm v1.9.16
	github.com/json-iterator/go v1.1.10
	github.com/lestrrat-go/strftime v1.0.5
	github.com/pkg/errors v0.9.1
	github.com/rifflock/lfshook v0.0.0-20180920164130-b9218ef580f5
	github.com/sirupsen/logrus v1.8.1
	github.com/smallnest/rpcx v1.7.1
	github.com/streadway/amqp v1.0.0
	golang.org/x/net v0.0.0-20210428140749-89ef3d95e781
)
