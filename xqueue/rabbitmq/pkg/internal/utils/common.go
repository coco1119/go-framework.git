package utils

import (
	"fmt"
	"gitee.com/coco1119/go-framework.git/xqueue/rabbitmq/pkg/external"
)

// CancelChannel 关闭RabbitMQ Channel
func CancelChannel(channel *external.XChannel, tag string) error {
	if err := channel.Cancel(tag, true); err != nil {
		if amqpError, ok := err.(*external.XError); ok {
			if amqpError.Code != external.ChannelError {
				return fmt.Errorf("AMQP connection channel close error: %w", err)
			}
		}
	}
	if err := channel.Close(); err != nil {
		return err
	}
	return nil
}
