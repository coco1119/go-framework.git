package xserialize

import jsoniter "github.com/json-iterator/go"

type Serializer interface {
	Marshal(v interface{}) ([]byte, error)
	Unmarshal(data []byte, v interface{}) error
}

var json = jsoniter.ConfigCompatibleWithStandardLibrary

var innerSerializer Serializer = &defaultSerializer{}

type defaultSerializer struct{}

func (s *defaultSerializer) Marshal(v interface{}) ([]byte, error) {
	return json.Marshal(v)
}

func (s *defaultSerializer) Unmarshal(data []byte, v interface{}) error {
	return json.Unmarshal(data, v)
}
