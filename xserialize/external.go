package xserialize

func SetSerializer(s Serializer) {
	innerSerializer = s
}

func Marshal(v interface{}) ([]byte, error) {
	return innerSerializer.Marshal(v)
}

func Unmarshal(data []byte, v interface{}) error {
	return innerSerializer.Unmarshal(data, v)
}
