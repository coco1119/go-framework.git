package xserialize

import (
	"encoding/base64"
	"gitee.com/coco1119/go-framework.git/xencrypt"
	"gitee.com/coco1119/go-framework.git/xlog"
	"gitee.com/coco1119/go-framework.git/xmessage"
	"reflect"
	"time"
)

// DecodeMsg 解消息
func DecodeMsg(msg []byte) (string, *xmessage.MsgSign, int16, []byte, bool, int64, string) {
	var msgBase xmessage.MsgBase
	err := Unmarshal(msg, &msgBase)
	if err != nil {
		xlog.Logger().Errorln(err)
		return "", nil, 0, []byte(""), false, 0, ""
	}
	return msgBase.Head, &msgBase.Sign, msgBase.ErrCode, []byte(msgBase.Data), true, msgBase.Uid, msgBase.IP
}

// EncodeMsg 加密消息
func EncodeMsg(msgHead string, errCode int16, v interface{}, encode int, encodeKey string, uid int64) []byte {
	var sign xmessage.MsgSign
	sign.Time = time.Now().Unix()
	sign.Encode = encode

	var msgBase xmessage.MsgBase
	msgBase.ErrCode = errCode
	msgBase.Head = msgHead
	msgBase.Sign = sign
	msgBase.Uid = uid

	if v != nil {
		kind := reflect.TypeOf(v).Kind()
		switch kind {
		case reflect.String:
			msgBase.Data = v.(string)
		default:
			msgBase.Data = JtoA(v)
		}

		if !xmessage.IsIgnoreMessageHeader(msgHead) {
			xlog.Logger().WithFields(xlog.Fields{
				"_head":   msgHead,
				"data":    msgBase.Data,
				"uid":     msgBase.Uid,
				"encode:": encode,
				"errcode": msgBase.ErrCode,
			}).Infoln("[SEND MESSAGE]")
		}

		// 需要做加密处理
		if encode == xencrypt.EncodeAes {
			datas, err := xencrypt.AesCTR_Encrypt([]byte(msgBase.Data), []byte(encodeKey))
			if err != nil {
				xlog.Logger().Errorln("encode msg filed:", err)
			} else {
				msgBase.Data = base64.URLEncoding.EncodeToString(datas)
			}
		}
	} else {
		if !xmessage.IsIgnoreMessageHeader(msgHead) {
			xlog.Logger().WithFields(xlog.Fields{
				"msgHead:": msgHead,
				"data":     msgBase.Data,
				"uid":      msgBase.Uid,
				"encode:":  msgBase.ErrCode,
			}).Infoln("[SEND NULL MESSAGE]")
		}
	}

	return JtoB(&msgBase)
}
