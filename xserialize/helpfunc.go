package xserialize

import (
	"gitee.com/coco1119/go-framework.git/xlog"
)

func JtoA(v interface{}) string {
	s, err := innerSerializer.Marshal(v)
	if err != nil {
		xlog.Logger().Errorln(err)
		return ""
	}
	return string(s)
}

func JtoB(v interface{}) []byte {
	s, err := innerSerializer.Marshal(v)
	if err != nil {
		xlog.Logger().Errorln(err)
		return nil
	}
	return s
}
