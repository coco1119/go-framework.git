package xdatabase

import (
	"fmt"
	"gitee.com/coco1119/go-framework.git/xgo"
	"gitee.com/coco1119/go-framework.git/xlog"
	"github.com/garyburd/redigo/redis"
	"github.com/pkg/errors"
	"strconv"
	"time"
)

// 注意，这里面这些写法很low，尽量不要用，直接用go-redis吧，公认最好的redis封装

type RedisError string

func (err RedisError) Error() string {
	return "Redis Error: " + string(err)
}

// Deprecated: Use go-redis instead.
type Redigo struct {
	// Deprecated: Use RedisV2 instead.
	pool *redis.Pool
}

func NewRedigo(ip string, db int, auth string) *Redigo {
	pRedisPool := &redis.Pool{
		MaxIdle:     50,
		MaxActive:   12000,
		IdleTimeout: 0,
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", ip)
			if err != nil {
				return nil, err
			}
			if auth != "" {
				//redis校验
				if _, err := c.Do("AUTH", auth); err != nil {
					c.Close()
					return nil, err
				}
			}

			if _, err := c.Do("SELECT", db); err != nil {
				c.Close()
				return nil, err
			}

			return c, err
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			_, err := c.Do("PING")
			return err
		},
	}

	xlog.Logger().Infoln("redis connect!", db)

	rdg := Redigo{pool: pRedisPool}
	return &rdg
}

func (client *Redigo) GlobalLock(key string, redisLockTimeout int, f func()) {
	//从连接池中娶一个con链接，pool可以自己定义。
	con := client.pool.Get()
	defer con.Close()
	for {
		if _, err := redis.String(con.Do("set", key, 1, "ex", redisLockTimeout, "nx")); err != nil {
			//间隔半秒去抢占锁
			time.Sleep(500 * time.Millisecond)
			continue
		}
		break
	}
	//业务逻辑
	xgo.DoSafely(f)
	//释放锁
	_, _ = con.Do("del", key)
}

func (client *Redigo) Close() {
	if client != nil {
		client.pool.Close()
		client.pool = nil
	}
}

// 数据库切换
func (client *Redigo) SelectDB(db uint8) error {
	cn := client.pool.Get()
	defer cn.Close()
	_, err := cn.Do("SELECT", db)
	return err
}

// key 相关方法
func (client *Redigo) Keys(pattern string) ([][]byte, error) {
	cn := client.pool.Get()
	defer cn.Close()

	res, err := cn.Do("KEYS", pattern)

	if err != nil {
		return nil, err
	}

	var vdatas [][]byte
	vres := res.([]interface{})
	for _, datas := range vres {
		var vdata []byte
		for _, vbyte := range datas.([]byte) {
			vdata = append(vdata, vbyte)
		}
		vdatas = append(vdatas, vdata)
	}

	//ret := make([]string, len(keydata))
	//for i, k := range keydata {
	//	ret[i] = string(k)
	//}

	return vdatas, nil
}

func (client *Redigo) Del(key string) (bool, error) {
	cn := client.pool.Get()
	defer cn.Close()

	res, err := cn.Do("DEL", key)
	if err != nil {
		return false, err
	}
	return res.(int64) == 1, nil
}

func (client *Redigo) Set(key string, val []byte) error {
	cn := client.pool.Get()
	defer cn.Close()

	_, err := cn.Do("SET", key, string(val))
	if err != nil {
		return err
	}
	return nil
}

func (client *Redigo) Get(key string) ([]byte, error) {
	cn := client.pool.Get()
	defer cn.Close()

	res, err := cn.Do("GET", key)
	if err != nil {
		return nil, err
	}
	if res == nil {
		return nil, RedisError("Key `" + key + "` does not exist")
	}

	data := res.([]byte)
	return data, nil
}

func (client *Redigo) Exists(key string) bool {
	cn := client.pool.Get()
	defer cn.Close()

	v, err := cn.Do("EXISTS", key)
	if err != nil {
		return false
	}
	return v.(int64) > 0
}

func (client *Redigo) Llen(key string) (int, error) {
	cn := client.pool.Get()
	defer cn.Close()

	res, err := cn.Do("LLEN", key)
	if err != nil {
		return -1, err
	}
	return int(res.(int64)), nil
}

func (client *Redigo) Lrange(key string, start int, end int) ([][]byte, error) {
	cn := client.pool.Get()
	defer cn.Close()

	res, err := cn.Do("LRANGE", key, strconv.Itoa(start), strconv.Itoa(end))
	if err != nil {
		return nil, err
	}

	var vdatas [][]byte
	vres := res.([]interface{})
	for _, datas := range vres {
		var vdata []byte
		for _, vbyte := range datas.([]byte) {
			vdata = append(vdata, vbyte)
		}
		vdatas = append(vdatas, vdata)
	}

	return vdatas, nil
}

func (client *Redigo) Lindex(key string, index int) ([]byte, error) {
	cn := client.pool.Get()
	defer cn.Close()

	res, err := cn.Do("LINDEX", key, strconv.Itoa(index))
	if err != nil {
		return nil, err
	}
	return res.([]byte), nil
}

func (client *Redigo) Lset(key string, index int, value []byte) error {
	cn := client.pool.Get()
	defer cn.Close()

	_, err := cn.Do("LSET", key, strconv.Itoa(index), string(value))
	if err != nil {
		return err
	}
	return nil
}

func (client *Redigo) Lpush(key string, val []byte) error {
	cn := client.pool.Get()
	defer cn.Close()

	_, err := cn.Do("LPUSH", key, string(val))
	if err != nil {
		return err
	}
	return nil
}

func (client *Redigo) BLpop(key string) ([]interface{}, error) {
	cn := client.pool.Get()
	defer cn.Close()

	res, err := cn.Do("blpop", key, 100)
	if err != nil {
		return nil, err
	}

	if res == nil {
		return nil, nil
	}

	return res.([]interface{}), nil
}

func (client *Redigo) Lpop(key string) ([]byte, error) {
	cn := client.pool.Get()
	defer cn.Close()

	res, err := cn.Do("LPOP", key)
	if err != nil {
		return nil, err
	}

	if res == nil {
		return nil, nil
	}

	return res.([]byte), nil
}

func (client *Redigo) Rpush(key string, val []byte) error {
	cn := client.pool.Get()
	defer cn.Close()

	_, err := cn.Do("RPUSH", key, string(val))

	if err != nil {
		return err
	}
	return nil
}

func (client *Redigo) Rpop(key string) ([]byte, error) {
	cn := client.pool.Get()
	defer cn.Close()

	res, err := cn.Do("RPOP", key)
	if err != nil {
		return nil, err
	}
	return res.([]byte), nil
}

func (client *Redigo) Lrem(key string, value []byte) (int, error) {
	cn := client.pool.Get()
	defer cn.Close()

	res, err := cn.Do("LREM", key, 0, string(value))
	if err != nil {
		return -1, err
	}
	return int(res.(int64)), nil
}

//! sorted set 相关
func (client *Redigo) Zadd(key string, value []byte, score float64) (bool, error) {
	cn := client.pool.Get()
	defer cn.Close()

	res, err := cn.Do("ZADD", key, strconv.FormatFloat(score, 'f', -1, 64), string(value))
	if err != nil {
		return false, err
	}

	return res.(int64) == 1, nil
}

func (client *Redigo) Zrem(key string, value []byte) (bool, error) {
	cn := client.pool.Get()
	defer cn.Close()

	res, err := cn.Do("ZREM", key, string(value))
	if err != nil {
		return false, err
	}

	return res.(int64) == 1, nil
}

func (client *Redigo) Zincrby(key string, value []byte, score float64) (float64, error) {
	cn := client.pool.Get()
	defer cn.Close()

	res, err := cn.Do("ZINCRBY", key, strconv.FormatFloat(score, 'f', -1, 64), string(value))
	if err != nil {
		return 0, err
	}

	data := string(res.([]byte))
	f, _ := strconv.ParseFloat(data, 64)
	return f, nil
}

func (client *Redigo) Zrange(key string, start int, end int) ([][]byte, error) {
	cn := client.pool.Get()
	defer cn.Close()

	res, err := cn.Do("ZRANGE", key, strconv.Itoa(start), strconv.Itoa(end))
	if err != nil {
		return nil, err
	}

	var vdatas [][]byte
	vres := res.([]interface{})
	for _, datas := range vres {
		if datas != nil {
			var vdata []byte
			for _, vbyte := range datas.([]byte) {
				vdata = append(vdata, vbyte)
			}
			vdatas = append(vdatas, vdata)
		}
	}

	return vdatas, nil
}

func (client *Redigo) Zrevrange(key string, start int, end int) ([][]byte, error) {
	cn := client.pool.Get()
	defer cn.Close()

	res, err := cn.Do("ZREVRANGE", key, strconv.Itoa(start), strconv.Itoa(end))
	if err != nil {
		return nil, err
	}

	var vdatas [][]byte
	vres := res.([]interface{})
	for _, datas := range vres {
		if datas != nil {
			var vdata []byte
			for _, vbyte := range datas.([]byte) {
				vdata = append(vdata, vbyte)
			}
			vdatas = append(vdatas, vdata)
		}
	}

	return vdatas, nil
}

func (client *Redigo) ZrevrangeWithScore(key string, start int, end int) ([][]byte, error) {
	cn := client.pool.Get()
	defer cn.Close()

	res, err := cn.Do("ZREVRANGE", key, strconv.Itoa(start), strconv.Itoa(end), "WITHSCORES")
	if err != nil {
		return nil, err
	}

	var vdatas [][]byte
	vres := res.([]interface{})
	for _, datas := range vres {
		if datas != nil {
			var vdata []byte
			for _, vbyte := range datas.([]byte) {
				vdata = append(vdata, vbyte)
			}
			vdatas = append(vdatas, vdata)
		}
	}

	return vdatas, nil
}

//! hash 相关

func (client *Redigo) HSet(key string, field string, value []byte) error {
	cn := client.pool.Get()
	defer cn.Close()

	_, err := cn.Do("HSET", key, field, string(value))
	if err != nil {
		return err
	}

	return nil
}

func (client *Redigo) HGet(key string, field string) ([]byte, error) {
	cn := client.pool.Get()
	defer cn.Close()

	res, err := cn.Do("HGET", key, field)
	if err != nil {
		return nil, err
	}
	if res == nil {
		return nil, RedisError("Key `" + key + "` does not exist")
	}
	return res.([]byte), nil
}

func (client *Redigo) HExists(key string, field string) bool {
	cn := client.pool.Get()
	defer cn.Close()

	v, err := cn.Do("HEXISTS", key, field)
	if err != nil {
		return false
	}
	return v.(int64) > 0
}

func (client *Redigo) HMsetall(key string, obj interface{}) error {
	cn := client.pool.Get()
	defer cn.Close()

	_, err := cn.Do("HMSET", redis.Args{}.Add(key).AddFlat(obj)...)
	if err != nil {
		return err
	}

	return nil
}

func (client *Redigo) HMset(key string, args ...interface{}) error {
	cn := client.pool.Get()
	defer cn.Close()

	args = append([]interface{}{key}, args...)
	_, err := cn.Do("HMSET", args...)
	if err != nil {
		fmt.Println(err)
		return err
	}

	return nil
}

func (client *Redigo) HMget(key string, field string) ([][]byte, error) {
	cn := client.pool.Get()
	defer cn.Close()

	res, err := cn.Do("HMGET", key, field)
	if err != nil {
		return nil, err
	}

	var vdatas [][]byte
	vres := res.([]interface{})
	for _, datas := range vres {
		if datas != nil {
			var vdata []byte
			for _, vbyte := range datas.([]byte) {
				vdata = append(vdata, vbyte)
			}
			vdatas = append(vdatas, vdata)
		}
	}

	return vdatas, nil
}

func (client *Redigo) Hgetall(key string, obj interface{}) error {
	cn := client.pool.Get()
	defer cn.Close()

	v, err := redis.Values(cn.Do("HGETAll", key))
	if err != nil {
		return err
	}

	if len(v) == 0 {
		return RedisError(fmt.Sprintf("[key: %s] is not exists", key))
	}

	if err := redis.ScanStruct(v, obj); err != nil {
		return err
	}

	return nil
}

func (client *Redigo) Hvals(key string) ([][]byte, error) {
	cn := client.pool.Get()
	defer cn.Close()

	res, err := cn.Do("HVALS", key)
	if err != nil {
		return nil, err
	}

	var vdatas [][]byte
	vres := res.([]interface{})
	for _, datas := range vres {
		var vdata []byte
		for _, vbyte := range datas.([]byte) {
			vdata = append(vdata, vbyte)
		}
		vdatas = append(vdatas, vdata)
	}

	return vdatas, nil
}

func (client *Redigo) HDel(key string, field string) error {
	cn := client.pool.Get()
	defer cn.Close()

	_, err := cn.Do("HDEL", key, field)

	return err
}

func (client *Redigo) Hlen(key string) (int, error) {
	cn := client.pool.Get()
	defer cn.Close()

	res, err := cn.Do("HLEN", key)
	if err != nil {
		return -1, err
	}
	return int(res.(int64)), nil
}

func (client *Redigo) HIncrBy(key, field string, count int64) error {
	cn := client.pool.Get()
	defer cn.Close()

	_, err := cn.Do("HINCRBY", key, field, count)
	return err
}

//! 设置数据过期时间
func (client *Redigo) Expire(key string, second int64) error {
	cn := client.pool.Get()
	defer cn.Close()

	_, err := cn.Do("EXPIRE", key, second)

	return err
}

/* Stream */
// 添加
type XaddMsg struct {
	Key   string // 表名
	Value string // 数据结构json
}

// 写入数据到队列
func (client *Redigo) Xadd(key string, object *XaddMsg) error {
	cn := client.pool.Get()
	defer cn.Close()

	_, err := cn.Do("XADD", key, "*", object.Key, object.Value)
	if err != nil {
		return err
	}

	return nil
}

// 创建消费者组
func (client *Redigo) Xgroup(key string, name string, id string) error {
	cn := client.pool.Get()
	defer cn.Close()

	_, err := cn.Do("XGROUP", "CREATE", key, name, id)
	if err != nil {
		return err
	}

	return nil
}

// 从消费组队列读一条最新且未被消费的数据(消息到目前为止从未传递给其他消费者, 但是不会取到旧的历史消息)
func (client *Redigo) Xreadgroup(groupName, customerName, key, id string) (*XaddMsg, string, error) {
	cn := client.pool.Get()
	defer cn.Close()

	datas, err := redis.Values(cn.Do("XREADGROUP", "GROUP", groupName, customerName, "block", 0, "streams", key, ">"))
	if err != nil {
		return nil, "", err
	}

	// 只取一条
	if len(datas) > 0 {
		var keyInfo = datas[0].([]interface{})
		var idList = keyInfo[1].([]interface{})

		if len(idList) > 0 {
			var idInfo = idList[0].([]interface{})
			var id = string(idInfo[0].([]byte))
			var fieldList = idInfo[1].([]interface{})
			var field = string(fieldList[0].([]byte))
			var value = string(fieldList[1].([]byte))

			object := &XaddMsg{
				Key:   field,
				Value: value,
			}
			return object, id, nil
		}
	}

	err = errors.New("can't find stream")
	return nil, "", err
}

// 从指定stream队列根据id顺序获取一条消息(阻塞)
func (client *Redigo) Xread(key, id string) (*XaddMsg, string, error) {
	cn := client.pool.Get()
	defer cn.Close()

	datas, err := redis.Values(cn.Do("XREAD", "block", 0, "count", 1, "streams", key, id))
	if err != nil {
		return nil, "", err
	}

	// 只取一条
	if len(datas) > 0 {
		var keyInfo = datas[0].([]interface{})
		var idList = keyInfo[1].([]interface{})

		if len(idList) > 0 {
			var idInfo = idList[0].([]interface{})
			var id = string(idInfo[0].([]byte))
			var fieldList = idInfo[1].([]interface{})
			var field = string(fieldList[0].([]byte))
			var value = string(fieldList[1].([]byte))

			object := &XaddMsg{
				Key:   field,
				Value: value,
			}
			return object, id, nil
		}
	}

	err = errors.New("can't find stream")
	return nil, "", err
}

func (client *Redigo) Xack(key, groupName, id string) error {
	cn := client.pool.Get()
	defer cn.Close()

	_, err := cn.Do("XACK", key, groupName, id)
	if err != nil {
		return err
	}

	return nil
}

func (client *Redigo) Xdel(key, id string) error {
	cn := client.pool.Get()
	defer cn.Close()

	_, err := cn.Do("XDEL", key, id)
	if err != nil {
		return err
	}

	return nil
}

func (client *Redigo) Sadd(key, val interface{}) error {
	cn := client.pool.Get()
	defer cn.Close()
	_, err := cn.Do("Sadd", key, val)
	return err
}

func (client *Redigo) SADDS(key interface{}, val []int64) error {
	cn := client.pool.Get()
	defer cn.Close()
	interfaceStruct := []interface{}{key}
	for _, item := range val {
		interfaceStruct = append(interfaceStruct, item)
	}
	_, err := cn.Do("Sadd", interfaceStruct...)
	return err
}
func (client *Redigo) SRem(key, val interface{}) error {
	cn := client.pool.Get()
	defer cn.Close()
	_, err := cn.Do("SREM", key, val)
	return err
}

func (client *Redigo) SisMember(key, val interface{}) bool {
	cn := client.pool.Get()
	defer cn.Close()
	res, err := cn.Do("SISMEMBER", key, val)
	if err != nil {
		fmt.Println(err)
		return false
	}
	return res.(int64) == 1
}

func (client *Redigo) SMembers(key string) ([]interface{}, error) {
	cn := client.pool.Get()
	defer cn.Close()
	res, err := cn.Do("SMembers", key)
	if err != nil {
		fmt.Println(err)
	}
	return res.([]interface{}), nil

}
