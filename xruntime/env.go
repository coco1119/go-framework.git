package xruntime

import "gitee.com/coco1119/go-framework.git/xlog"

var environment Environment

// Environment 环境信息
type Environment string

func (e Environment) String() string {
	return string(e)
}

const (
	DevelopEnvironment    Environment = "dev"
	TestEnvironment       Environment = "test"
	ImageEnvironment      Environment = "image"
	ProductionEnvironment Environment = "pro"
)

var EnvironmentNames = map[string]Environment{
	"dev":   DevelopEnvironment,
	"test":  TestEnvironment,
	"image": ImageEnvironment,
	"pro":   ProductionEnvironment,
}

func SetEnvironment(e string) {
	env, ok := EnvironmentNames[e]
	if !ok {
		env = DevelopEnvironment
		xlog.Logger().Warnf("set invalid environment [%q], The default is the development environment[%q]", e, env)
	}
	environment = env
}

func GetEnvironment() Environment {
	env, ok := EnvironmentNames[environment.String()]
	if !ok {
		env = DevelopEnvironment
		xlog.Logger().Warnf("get invalid environment [%q], The default is the development environment[%q]", environment, env)
	}
	return env
}
