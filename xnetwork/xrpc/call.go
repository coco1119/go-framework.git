package xrpc

import (
	"context"
	"gitee.com/coco1119/go-framework.git/xlog"
	"gitee.com/coco1119/go-framework.git/xmessage"
	"github.com/smallnest/rpcx/client"
	"runtime/debug"
	"time"
)

func NewXClientWithAddr(addr string) client.XClient {
	return InitClient(addr, "ServerMethod", &CliPlugin{})
}

func Call(args *xmessage.RpcArgs, cli client.XClient) ([]byte, error) {
	var reply []byte
	doneChan := make(chan error, 1)
	c, cancel := context.WithCancel(context.Background())
	go func(s []byte) {
		select {
		case <-time.After(3 * time.Second):
			cancel()
			xlog.Logger().Errorf("error:cli:%+v", cli)
			xlog.Logger().Errorf("error:time out:%s", s)
		case err := <-doneChan:
			if err != nil {
				reply = []byte(err.Error())
				xlog.Logger().Errorf("call rpc done with error: %s", err)
			} else {
				xlog.Logger().Debug("call rpc suc")
			}
			break
		}
	}(debug.Stack())
	err := cli.Call(c, "ServerMsg", args, &reply)
	if err != nil {
		xlog.Logger().Errorf("first call rpc error:%s, go to reconnect...", err)
	}
	doneChan <- err
	return reply, err
}
