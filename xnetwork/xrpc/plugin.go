package xrpc

import (
	"gitee.com/coco1119/go-framework.git/xlog"
	"net"
)

//CliPlugin rpcx客户端组件
type CliPlugin struct {
}

func (p *CliPlugin) ClientConnected(conn net.Conn) (net.Conn, bool) {
	xlog.Logger().Infof("server %v connected", conn.RemoteAddr().String())
	return conn, true
}

func (p *CliPlugin) ClientConnectionClose(conn net.Conn) bool {
	xlog.Logger().Infof("server %v closed", conn.RemoteAddr().String())
	return true
}

type ServerPlugin struct {
}

func (p *ServerPlugin) HandleConnAccept(conn net.Conn) (net.Conn, bool) {
	xlog.Logger().Infof("server %v connected", conn.RemoteAddr().String())
	return conn, true
}

func (p *ServerPlugin) HandleConnClose(conn net.Conn) bool {
	xlog.Logger().Infof("server %v closed", conn.RemoteAddr().String())
	return true
}
