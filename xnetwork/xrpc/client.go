package xrpc

import (
	"context"
	"fmt"
	"gitee.com/coco1119/go-framework.git/xlog"
	"gitee.com/coco1119/go-framework.git/xmessage"
	"runtime/debug"
	"strings"
	"time"

	"github.com/smallnest/rpcx/client"
	"github.com/smallnest/rpcx/server"
)

type Client struct {
	cli client.XClient
}

func (c *Client) Call(in *xmessage.RpcArgs) (out []byte, err error) {
	doneChan := make(chan error, 1)
	ctx, cancel := context.WithCancel(context.Background())
	go func(s []byte) {
		select {
		case <-time.After(3 * time.Second):
			cancel()
			xlog.Logger().Errorf("error:cli:%+v", c.cli)
			xlog.Logger().Errorf("error:time out:%s", s)
		case err := <-doneChan:
			if err != nil {
				out = []byte(err.Error())
				xlog.Logger().Errorf("call rpc done with error: %s", err)
			} else {
				xlog.Logger().Debug("call rpc suc")
			}
			break
		}
	}(debug.Stack())
	err = c.cli.Call(ctx, "ServerMsg", in, &out)
	if err != nil {
		xlog.Logger().Errorf("first call rpc error:%s, go to reconnect...", err)
	}
	doneChan <- err
	return
}

func NewClientWithAddr(addr string) *Client {
	cli := InitClient(addr, "ServerMethod", &CliPlugin{})
	return &Client{cli: cli}
}

func NewPeer2PeerDiscovery(server, metadata string) client.ServiceDiscovery {
	ret, _ := client.NewPeer2PeerDiscovery(server, metadata)
	return ret
}

func InitClient(ipstr, serPath string, inPlug interface{}) client.XClient {
	ip := strings.Split(ipstr, ":")
	if len(ip) < 2 {
		xlog.Logger().Panic("rpcx InitClient: invalid ip=", ipstr)
		return nil
	}
	opt := client.DefaultOption
	opt.Heartbeat = false
	opt.HeartbeatInterval = 3 * time.Second
	opt.Retries = 1
	cli := client.NewXClient(serPath, client.Failtry, client.RandomSelect,
		NewPeer2PeerDiscovery(fmt.Sprintf("tcp@%s:1%s", ip[0], ip[1]), ""), opt)
	plug := client.NewPluginContainer()
	if inPlug == nil {
		inPlug = &CliPlugin{}
	}
	plug.Add(inPlug)
	cli.SetPlugins(plug)
	return cli
}

func NewRpcxServer(ipstr, serPath string, rvcr, inPlug interface{}) {
	s := server.NewServer()
	if inPlug == nil {
		inPlug = &CliPlugin{}
	}
	s.Plugins.Add(inPlug)
	ip := strings.Split(ipstr, ":")
	s.RegisterName(serPath, rvcr, "")
	err := s.Serve("tcp", fmt.Sprintf("0.0.0.0:1%s", ip[1]))
	if err != nil {
		if server.ErrServerClosed != err {
			panic(err)
		}
		xlog.Logger().Error("closed:", err)
	}
}
