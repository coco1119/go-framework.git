package xrouter

import (
	"net/http"
	"strings"
)

func GetHttpIP(req *http.Request) string {
	ip := req.Header.Get("Remote_addr")
	if ip == "" {
		ip = req.RemoteAddr
	}
	return strings.Split(ip, ":")[0]
}

//// GetCommonMsg 消息解密
//func GetCommonMsg(msg *xmessage.MsgHeader) error {
//	switch msg.Sign.Encode {
//	case xencrypt.EncodeNone: // 不加密不处理
//	case xencrypt.EncodeAes: // aes + base64
//		data, err := base64.URLEncoding.DecodeString(msg.Data)
//		if err != nil {
//			return err
//		}
//
//		bytes, err := xencrypt.AesCTR_Decrypt(data, []byte(httpWorker.EncodePhpKey))
//		if err != nil {
//			return err
//		}
//		msg.Data = string(bytes)
//	}
//	return nil
//}
//
//// GetErrReturn 获取错误返回结果
//func GetErrReturn(msgType string, err xerror.XError) []byte {
//	resultHead := new(xmessage.MsgHeader)
//	resultHead.Header = msgType
//	//resultHead.Data = err.Error()
//	resultHead.ErrCode = err.Code()
//	resultHead.Sign.Encode = httpWorker.Encode
//	resultHead.Sign.Time = time.Now().Unix()
//
//	xlog.Logger().WithFields(xlog.Fields{
//		"errhead": msgType,
//		"errmsg":  err.Error(),
//		"errcode": err.Code,
//	}).Infoln("[SEND ERROR HTTP RESP]")
//
//	switch resultHead.Sign.Encode {
//	case xencrypt.EncodeNone: // 不加密
//		resultHead.Data = err.Error()
//	case xencrypt.EncodeAes: // aes + base64
//		bytes, _ := xencrypt.AesCTR_Encrypt([]byte(err.Error()), []byte(httpWorker.EncodePhpKey))
//		resultHead.Data = base64.URLEncoding.EncodeToString(bytes)
//	}
//
//	return xserialize.JtoB(resultHead)
//}
//
//// GetCommonResp 获取通用返回结果
//func GetCommonResp(msgType string, v interface{}) *xmessage.MsgHeader {
//	resp := new(xmessage.MsgHeader)
//	resp.Header = msgType
//	resp.Sign.Encode = httpWorker.Encode
//	resp.Sign.Time = time.Now().Unix()
//
//	xlog.Logger().WithFields(xlog.Fields{
//		"head": msgType,
//		"data": xserialize.JtoA(v),
//	}).Infoln("[SEND HTTP RESP]")
//
//	switch resp.Sign.Encode {
//	case xencrypt.EncodeNone: // 不加密
//		resp.Data = xserialize.JtoA(v)
//	case xencrypt.EncodeAes: // aes + base64
//		var bytes []byte
//		if reflect.TypeOf(v).Kind() == reflect.String {
//			bytes, _ = xencrypt.AesCTR_Encrypt([]byte(v.(string)), []byte(httpWorker.EncodePhpKey))
//		} else {
//			bytes, _ = xencrypt.AesCTR_Encrypt([]byte(xserialize.JtoA(v)), []byte(httpWorker.EncodePhpKey))
//		}
//		resp.Data = base64.URLEncoding.EncodeToString(bytes)
//	}
//	return resp
//}
