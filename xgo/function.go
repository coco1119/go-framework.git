package xgo

import (
	"gitee.com/coco1119/go-framework.git/xlog"
	"runtime/debug"
)

func DoSafely(f func()) {
	defer func() {
		x := recover()
		if x != nil {
			xlog.Logger().WithFields(xlog.Fields{
				xlog.PanicMark: "DoSafely recover from panic",
			}).Errorln(x, string(debug.Stack()))
		}
	}()
	f()
}

func GoroutineSafely(f func()) {
	go func() {
		for {
			func() {
				defer func() {
					x := recover()
					if x != nil {
						xlog.Logger().WithFields(xlog.Fields{
							xlog.PanicMark: "GoroutineSafely recover from panic",
						}).Errorln(x, string(debug.Stack()))
					}
				}()
				f()
			}()
		}
	}()
}
