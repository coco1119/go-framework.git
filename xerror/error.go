package xerror

import "fmt"

type Error struct {
	code int16
	msg  string
}

func NewXError(msg string) XError {
	return &Error{
		code: -1,
		msg:  msg,
	}
}

func NewXErrorWithCode(code int16, msg string) XError {
	return &Error{
		code: code,
		msg:  msg,
	}
}

func (e *Error) Error() string {
	if e != nil {
		return e.msg
	}
	return "nil error"
}

func (e *Error) String() string {
	if e != nil {
		return fmt.Sprintf("err code=%d, msg=%s", e.code, e.msg)
	}
	return "nil error"
}

func (e *Error) GetCode() int16 {
	if e != nil {
		return e.code
	}
	return 0
}

func (e *Error) GetMsg() string {
	if e != nil {
		return e.msg
	}
	return "nil error"
}
