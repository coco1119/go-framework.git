package xerror

import "fmt"

type XError interface {
	error
	fmt.Stringer
	GetCode() int16
	GetMsg() string
}
