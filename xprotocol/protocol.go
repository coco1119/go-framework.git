package xprotocol

import (
	"reflect"
)

type ProtocolHandler func(...interface{}) (int16, interface{})

type ProtocolInfo struct {
	ProtoType    reflect.Type
	ProtoHandler ProtocolHandler
}

type ProtocolWorker struct {
	FuncProtocol map[string]*ProtocolInfo
}

func (self *ProtocolWorker) RegisterMessage(header string, proto interface{}, handler ProtocolHandler) {
	var info ProtocolInfo
	info.ProtoType = reflect.TypeOf(proto)
	info.ProtoHandler = handler
	if self.FuncProtocol == nil {
		self.FuncProtocol = make(map[string]*ProtocolInfo)
	}
	self.FuncProtocol[header] = &info
}

func (self *ProtocolWorker) GetProtocol(protocol string) *ProtocolInfo {
	return self.FuncProtocol[protocol]
}
