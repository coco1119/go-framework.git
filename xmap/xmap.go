package xmap

import "sync"

type XMap struct {
	data map[interface{}]interface{}
	mu   *sync.RWMutex
}

func NewXMap() *XMap {
	return &XMap{
		data: make(map[interface{}]interface{}),
		mu:   new(sync.RWMutex),
	}
}

func NewXMapLen(len int) *XMap {
	return &XMap{
		data: make(map[interface{}]interface{}, len),
		mu:   new(sync.RWMutex),
	}
}

func (m *XMap) Len() int {
	m.mu.RLock()
	r := len(m.data)
	m.mu.RUnlock()
	return r
}

func (m *XMap) Keys() []interface{} {
	var ds []interface{}
	m.mu.RLock()
	length := len(m.data)
	if length != 0 {
		ds = make([]interface{}, length)
		i := 0
		for k := range m.data {
			ds[i] = k
			i++
		}
	}
	m.mu.RUnlock()
	return ds
}

func (m *XMap) Values() []interface{} {
	var ds []interface{}
	m.mu.RLock()
	length := len(m.data)
	if length != 0 {
		ds = make([]interface{}, length)
		i := 0
		for _, v := range m.data {
			ds[i] = v
			i++
		}
	}
	m.mu.RUnlock()
	if length == 0 {
		return nil
	}
	return ds
}

func (m *XMap) KeyValues() map[interface{}]interface{} {
	m.mu.RLock()
	var ds map[interface{}]interface{}
	ds = make(map[interface{}]interface{}, len(m.data))
	for k, v := range m.data {
		ds[k] = v
	}
	m.mu.RUnlock()
	return ds
}

func (m *XMap) gkey(k interface{}) interface{} {
	if v, ok := k.(int); ok {
		return int64(v)
	}

	if v, ok := k.(int32); ok {
		return int64(v)
	}

	return k
}

func (m *XMap) HasKey(k interface{}) bool {
	m.mu.RLock()
	_, ok := m.data[m.gkey(k)]
	m.mu.RUnlock()
	return ok
}

func (m *XMap) Get(k interface{}) interface{} {
	m.mu.RLock()
	p := m.data[m.gkey(k)]
	m.mu.RUnlock()
	return p
}

func (m *XMap) Add(k interface{}, v interface{}) {
	m.mu.Lock()
	m.data[m.gkey(k)] = v
	m.mu.Unlock()
}

func (m *XMap) Remove(k interface{}) {
	m.mu.Lock()
	delete(m.data, m.gkey(k))
	m.mu.Unlock()
}

func (m *XMap) GetOk(k interface{}) (interface{}, bool) {
	m.mu.RLock()
	p, ok := m.data[m.gkey(k)]
	m.mu.RUnlock()
	return p, ok
}

func (m *XMap) Clear() {
	m.mu.Lock()
	m.data = make(map[interface{}]interface{})
	m.mu.Unlock()
}
